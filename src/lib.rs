#![feature(globs)]

extern crate libc;
extern crate xlib;

pub use egl::*;

mod egl;